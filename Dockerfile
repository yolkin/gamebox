FROM node

RUN apt-get update

RUN mkdir /game
COPY package.json /game
WORKDIR /game
RUN yarn install

COPY . /game


RUN yarn build

CMD yarn start

EXPOSE 3000
